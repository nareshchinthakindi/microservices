package com.ms.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Max;
import java.util.Objects;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class BasicUserDto {
    protected Long id;

    @NonNull
    @Max(value = 100, message = "USER_NAME_TOO_LONG")
    protected String userName;

    @NonNull
    @Max(value = 45)
    protected String firstName;

    @NonNull
    protected String lastName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicUserDto that = (BasicUserDto) o;
        return id.equals(that.id) && userName.equals(that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName);
    }

    @Override
    public String toString() {
        return "BasicUserDto{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }
}
