package com.ms.dto;

import com.ms.model.Client;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import java.util.List;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class UserDto extends BasicUserDto {

    private String password;

    @NonNull
    @Email
    private String email;

    @NonNull
    private Boolean active;

    private Client client;

    private List<RoleDto> roles;
}
