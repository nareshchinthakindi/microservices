package com.ms.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

@Component
public class ThreadPoolManager {

//    @Value("${rabbitmq.core.pool.size:5}")
//    private Integer corePoolSize;
//
//    @Value("${rabbitmq.max.pool.size:10}")
//    private Integer maxPoolSize;
//
//    @Value("${rabbitmq.queue.capacity.size:25}")
//    private Integer queueCapacity;
//
//    @Bean(name = "B2BThreadPoolTaskExecutorBean")
//    public Executor taskExecutor() {
//        final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
//        taskExecutor.setCorePoolSize(corePoolSize);
//        taskExecutor.setQueueCapacity(queueCapacity);
//        taskExecutor.setMaxPoolSize(maxPoolSize);
//        taskExecutor.setThreadNamePrefix("RabbitMQ Message -- ");
//        taskExecutor.initialize();
//        return taskExecutor;
//    }
}
