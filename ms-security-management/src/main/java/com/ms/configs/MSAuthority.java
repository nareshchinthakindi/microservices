package com.ms.configs;

import org.springframework.security.core.GrantedAuthority;

public class MSAuthority implements GrantedAuthority {

    private final String authority;

    public MSAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
