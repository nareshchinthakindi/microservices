package com.ms.repository;

import com.ms.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {

    @Query("SELECT user from User user JOIN user.client client WHERE user.userName=?1")
    Optional<User> findByUserName(String userName);

    @Query("SELECT user FROM User user JOIN user.roles roles JOIN user.client client WHERE roles.roleName=?1 AND client.id=?2")
    List<User> findUsersByRoleName(String roleName, Long clientId);

    @Query("SELECT user FROM User user JOIN user.roles roles JOIN user.client client " +
            "WHERE user.id=?1 AND roles.id=?2 AND client.id=?3")
    User findUserByRoleNameAndClientId(Long userId, Long roleId, Long clientId);

    User findByClientIdAndId(Long clientId, Long userId);
}
