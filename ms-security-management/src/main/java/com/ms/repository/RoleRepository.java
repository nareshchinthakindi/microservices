package com.ms.repository;

import com.ms.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository  extends PagingAndSortingRepository<Role, Long> {
    @Query("SELECT role FROM Role role JOIN role.users users WHERE users.userName=?1")
    List<Role> findRoleByUserName(String userName);
}
