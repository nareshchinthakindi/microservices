package com.ms.service;

import com.ms.dto.LoginResult;
import com.ms.exceptions.InvalidAuthenticationToken;
import com.ms.util.JwtHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationUserService {

    @Autowired
    private JwtHelper jwtHelper;

    public LoginResult refreshToken(Authentication authentication) {
        OAuth2TokenValidatorResult validatorResult = jwtHelper.isValidToken(authentication);

        if(validatorResult.hasErrors()) {
            throw new InvalidAuthenticationToken();
        }

        return new LoginResult(jwtHelper.refreshToken(authentication));
    }
}
