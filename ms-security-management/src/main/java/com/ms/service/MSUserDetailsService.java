package com.ms.service;

import com.ms.configs.MSAuthority;
import com.ms.model.Role;
import com.ms.model.User;
import com.ms.repository.RoleRepository;
import com.ms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component("MSUserDetailsService")
public class MSUserDetailsService extends InMemoryUserDetailsManager {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    public MSUserDetailsService() {
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(username);
        User msUser = user.orElse(null);
        if(user.isPresent()) {
            return org.springframework.security.core.userdetails.User
                    .withUsername(msUser.getUserName())
                    .authorities(buildAuthorities(
                            roleRepository.findRoleByUserName(msUser.getUserName())
                    ))
                    .password(msUser.getPassword())
                    .build();
        } else {
            throw new UsernameNotFoundException("User Name Not Found...");
        }
    }
    private List<MSAuthority> buildAuthorities(List<Role> roles) {
        return roles.stream().map(role -> new MSAuthority(role.getRoleName()))
                .collect(Collectors.toList());
    }
}
