package com.ms.service;

import com.ms.dto.UserDto;
import com.ms.exceptions.UserExistsException;
import com.ms.model.User;
import com.ms.repository.UserRepository;
import com.ms.util.EntityConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityConverter entityConverter;

    public UserDto getUser(String userName) {
        User user = userRepository.findByUserName(userName).orElseThrow(() -> new UserExistsException("User Not Found"));
        return entityConverter.convertToDto(UserDto.class, user);
    }
}
