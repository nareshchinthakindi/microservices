package com.ms.error;

public class InvalidDataException extends RuntimeException{
    public InvalidDataException(String errorMsg){
        super(errorMsg);
    }
}
