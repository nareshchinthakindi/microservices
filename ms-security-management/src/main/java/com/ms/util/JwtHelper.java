package com.ms.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.ms.exceptions.InvalidAuthenticationToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.util.*;

/**
 * A helper class to create JWTs
 */
@Slf4j
@Component
public class JwtHelper {

    private final RSAPrivateKey privateKey;
    private final RSAPublicKey publicKey;

    @Value("${app.security.jwt.token.timeout.period}")
    private int timeout;
    @Value("${app.security.jwt.token.issuer}")
    private String issuer;


    public JwtHelper(RSAPrivateKey privateKey, RSAPublicKey publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String createJwtForClaims(String subject, Map<String, String> claims) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Instant.now().toEpochMilli());
        calendar.add(Calendar.MINUTE, timeout);

        JWTCreator.Builder jwtBuilder = JWT.create()
                .withSubject(subject);

        // Add claims
        claims.forEach(jwtBuilder::withClaim);
        // Add expiredAt and etc
        return jwtBuilder
                .withIssuer(issuer)
                .withNotBefore(new Date())
                .withExpiresAt(calendar.getTime())
                .sign(Algorithm.RSA256(publicKey, privateKey));
    }

    public String refreshToken(Authentication authentication) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
        Map<String, Object> claimObjects = token.getToken().getClaims();
        Map<String, String> claims = new HashMap<>();
        claimObjects.forEach((key, value) -> claims.put(key, value.toString()));
        String subject = token.getToken().getSubject();
        return createJwtForClaims(subject, claims);
    }
    public String getAuthorizedUserName(Authentication authentication) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
        Map<String, Object> attributes = token.getTokenAttributes();
        Object userName = attributes.get(Constants.FIELD_USER_NAME);
        if(Objects.isNull(userName)) {
            throw new InvalidAuthenticationToken();
        }
        return userName.toString();
    }

    public OAuth2TokenValidatorResult isValidToken(Authentication authentication) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
        Jwt jwt = token.getToken();
        return JwtValidators.createDefaultWithIssuer(issuer).validate(jwt);
    }

    private Object getAuthenticationTokenAttribute(Authentication authentication, String attributeName) {
        JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
        Map<String, Object> attributes = token.getTokenAttributes();
        Object clientIdObj = attributes.get(attributeName);
        if(Objects.isNull(clientIdObj)) {
            throw new InvalidAuthenticationToken();
        }
        return clientIdObj;
    }

    public Long getClientId(Authentication authentication) {
        return Long.valueOf(getAuthenticationTokenAttribute(authentication, Constants.JWT_FIELD_CLIENT_ID).toString());
    }

    public String getRole(Authentication authentication){
        return getAuthenticationTokenAttribute(authentication, Constants.FIELD_AUTHORITIES).toString();
    }

    public Long getUserId(Authentication authentication){
        return Long.parseLong(getAuthenticationTokenAttribute(authentication, Constants.FIELD_USER_ID).toString());
    }

}