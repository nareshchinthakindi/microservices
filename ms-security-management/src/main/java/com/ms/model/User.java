package com.ms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Audited
@Data
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name ="user")
public class User extends BasicIdModel{


    @NotNull
    @Column(name = "user_name")
    private String userName;

    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @Email
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(name = "updated_date")
    @LastModifiedDate
    private Timestamp updatedDate;

    @NotNull
    @Column(name = "created_date")
    @CreatedDate
    private Timestamp createdDate;

    @NotNull
    private Boolean active;

    @ManyToOne(fetch = FetchType.EAGER,
    targetEntity = Client.class)
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
               joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return super.getId().equals(user.getId()) && userName.equals(user.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), userName);
    }
}
