package com.ms.controller;

import com.ms.dto.LoginDTO;
import com.ms.dto.LoginResult;
import com.ms.dto.UserDto;
import com.ms.model.Client;
import com.ms.service.AuthenticationUserService;
import com.ms.service.UserService;
import com.ms.util.Constants;
import com.ms.util.JwtHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/")
public class AuthController {

    @Autowired
    private JwtHelper jwtHelper;

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("MSUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationUserService authenticationUserService;


    @PostMapping(path = Constants.PATH_LOGIN)
    public LoginResult login(@RequestBody LoginDTO loginDTO) {

        UserDetails userDetails;

        String userName = loginDTO.getUserName();
        String password = loginDTO.getPassword();

        try {
            userDetails = userDetailsService.loadUserByUsername(userName);
            userDetails.getAuthorities().forEach(grantedAuthority -> {
                log.debug(grantedAuthority.getAuthority());
            });
        }catch (UsernameNotFoundException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User Not Found");
        }

        if (passwordEncoder.matches(password, userDetails.getPassword())) {
            Map<String, String> claims = new HashMap<>();
            claims.put(Constants.FIELD_USER_NAME, userName);
            UserDto userDto = userService.getUser(userName);
            claims.put(Constants.FIELD_USER_ID, userDto.getId().toString());
            Client client = userDto.getClient();

            if (null != client) {
                claims.put("client-id", client.getId().toString());
            }

            String authorities = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.joining(Constants.DEFAULT_DELIMITER));

            claims.put(Constants.FIELD_AUTHORITIES, authorities);

            String jwt = jwtHelper.createJwtForClaims(userName, claims);
            return new LoginResult(jwt);
        }

        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User Not Authenticated");
    }

}
