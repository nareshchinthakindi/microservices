package com.ms;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

import javax.ws.rs.core.HttpHeaders;


@Configuration
public class CorsConfiguration implements WebFluxConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
       registry.addMapping("/**")
               .allowCredentials(true)
               .allowedOrigins("*")
               .allowedMethods("*")
               .exposedHeaders(HttpHeaders.SET_COOKIE);
    }
}
