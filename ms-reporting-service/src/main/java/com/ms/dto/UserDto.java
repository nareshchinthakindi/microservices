package com.ms.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Email;
import java.util.List;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class UserDto extends BasicUserDto {

    private String password;

    @NonNull
    @Email
    private String email;

    @NonNull
    private Boolean active;


    private List<RoleDto> roles;
}
