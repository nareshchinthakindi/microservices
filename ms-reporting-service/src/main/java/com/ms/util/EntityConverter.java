package com.ms.util;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EntityConverter {

    @Autowired
    private Mapper mapper;

    public <F, T> T convertToDto(Class<T> clazz, F fromObject) {return mapper.map(fromObject, clazz);}

    public <F, T> List<T> convertToDtoList(Class<T> clazz, List<F> fromObject) {
        return fromObject.stream()
                .map(finding ->convertToDto(clazz, finding))
                .collect(Collectors.toList());
    }

    public <F, T> T convertFromDto(Class<T> clazz, F fromObject) {return mapper.map(fromObject, clazz);}

}
