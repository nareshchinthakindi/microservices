package com.ms.util;

public final class Constants {
    private Constants() throws IllegalAccessException {
        throw new IllegalAccessException("Can not create util instance");
    }

    public static final String FIELD_USER_NAME = "username";
    public static final String FIELD_ROLE_NAME = "roleName";
    public static final String FIELD_USER_ID = "userId";
    public static final String FIELD_ROLE_ID = "roleId";
    public static final String FIELD_AUTHORITIES = "authorities";
    public static final String PATH_USER = "/user";
    public static final String PATH_USER_BY_ROLE_NAME = PATH_USER + "/{" + FIELD_ROLE_NAME + "}";
    public static final String PATH_USER_BY_USER_ROLE = PATH_USER + "/{" + FIELD_USER_ID
            + "}/validate/{" + FIELD_ROLE_ID + "}";
    public static final String PATH_LOGIN = "login";
    public static final String DEFAULT_DELIMITER = ",";
    public static final String JWT_FIELD_CLIENT_ID = "client-id";
    public static final String PATH_EDIT_USER = PATH_USER + "/{" + FIELD_USER_ID + "}";
    public static final String PATH_PASSWORD= PATH_USER+"/password";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_MANAGER = "MANAGER";

    public static final String ROLE_USER = "USER";
    public static final String ROLE_SYSTEM = "SYSTEM";
    public static final String ROLE_CLIENTMANAGER = "CLIENTMANAGER";







}
