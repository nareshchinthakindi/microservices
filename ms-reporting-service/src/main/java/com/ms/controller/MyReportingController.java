package com.ms.controller;

import com.ms.util.JwtHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/reporting")
public class MyReportingController {

    @Autowired
    private JwtHelper jwtHelper;

    @GetMapping
    public String helloWorld(Authentication authentication) {
        log.info("Enter into test {}, {}", jwtHelper.getClientId(authentication), jwtHelper.getClientId(authentication));
        return jwtHelper.getAuthorizedUserName(authentication);
    }
}
