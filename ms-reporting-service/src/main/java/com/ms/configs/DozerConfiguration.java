package com.ms.configs;


import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerConfiguration {

    @Bean
    public DozerBeanMapper mapper() {
        DozerBeanMapper mapper = new  DozerBeanMapper();
        mapper.addMapping(objectMappingBuilder);
        return mapper;
    }

    final BeanMappingBuilder objectMappingBuilder = new BeanMappingBuilder() {
        @Override
        protected void configure() {

//            mapping(ClientAPiKey.class, ClientApiKeyDTO.class, TypeMappingOptions.mapNull(false))
//                    .fields("client.id", "clientId")
//                    .fields("client.name", "clientName")
//            ;

        }
    };
}